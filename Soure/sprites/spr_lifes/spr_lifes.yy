{
    "id": "34a4636b-7d9c-4f0f-8594-be25628436f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lifes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ba8f1e3-a441-444c-9ea0-2f361bff2b90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34a4636b-7d9c-4f0f-8594-be25628436f1",
            "compositeImage": {
                "id": "5a0dbdc2-7d1f-454d-8ff1-de535fdf842e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ba8f1e3-a441-444c-9ea0-2f361bff2b90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f96e8bbb-f158-40e2-843f-cffa477d8714",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ba8f1e3-a441-444c-9ea0-2f361bff2b90",
                    "LayerId": "5c948117-39ee-4be2-8816-c9cc2ea6b2dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5c948117-39ee-4be2-8816-c9cc2ea6b2dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34a4636b-7d9c-4f0f-8594-be25628436f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}