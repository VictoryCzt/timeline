{
    "id": "0fce8646-843d-4830-b489-b560d126ac4e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 1,
    "bbox_right": 7,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b68a8f63-f49d-4777-bfe1-4643ea68a021",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fce8646-843d-4830-b489-b560d126ac4e",
            "compositeImage": {
                "id": "f77d84c1-19dc-40c5-8a24-e62867a3927a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b68a8f63-f49d-4777-bfe1-4643ea68a021",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc59351a-7cf2-4591-9d6d-623b207e8e56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b68a8f63-f49d-4777-bfe1-4643ea68a021",
                    "LayerId": "60e201fa-536f-4607-acf0-cac3a0f85876"
                }
            ]
        },
        {
            "id": "a2202550-889b-428a-ab8e-c700a2e2e069",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fce8646-843d-4830-b489-b560d126ac4e",
            "compositeImage": {
                "id": "c75347d2-24aa-4e85-9d9b-f2d8dcdb0e2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2202550-889b-428a-ab8e-c700a2e2e069",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7050268-04a6-458d-8c37-9a632b489a41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2202550-889b-428a-ab8e-c700a2e2e069",
                    "LayerId": "60e201fa-536f-4607-acf0-cac3a0f85876"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "60e201fa-536f-4607-acf0-cac3a0f85876",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0fce8646-843d-4830-b489-b560d126ac4e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}