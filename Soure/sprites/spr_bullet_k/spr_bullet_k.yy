{
    "id": "73d6ca3e-0f21-470c-99b6-0f30c59df532",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet_k",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 3,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "614a22a9-1db4-4e1c-b014-f362e4817811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73d6ca3e-0f21-470c-99b6-0f30c59df532",
            "compositeImage": {
                "id": "0cc63bd2-67d6-43c7-8eb0-5f8b4b883a1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "614a22a9-1db4-4e1c-b014-f362e4817811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af654ff7-6a61-4361-ba20-df92b933540d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "614a22a9-1db4-4e1c-b014-f362e4817811",
                    "LayerId": "4441bcfd-d598-4c81-8dd0-2ed81994d10e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "4441bcfd-d598-4c81-8dd0-2ed81994d10e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73d6ca3e-0f21-470c-99b6-0f30c59df532",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 2,
    "yorig": 7
}