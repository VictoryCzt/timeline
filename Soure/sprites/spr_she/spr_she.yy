{
    "id": "7b70ccbf-d904-4d9e-b4bd-b8bdbc07e890",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_she",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9eebc05d-801c-41ec-9ef8-feb4c8cff994",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b70ccbf-d904-4d9e-b4bd-b8bdbc07e890",
            "compositeImage": {
                "id": "069e0450-1c56-4f05-b56f-bdc4cd95ed19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9eebc05d-801c-41ec-9ef8-feb4c8cff994",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "655ef6a2-b7ea-4341-b451-d13af7277440",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9eebc05d-801c-41ec-9ef8-feb4c8cff994",
                    "LayerId": "94130037-ef19-4339-a60e-79ef2f26c4d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "94130037-ef19-4339-a60e-79ef2f26c4d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b70ccbf-d904-4d9e-b4bd-b8bdbc07e890",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}