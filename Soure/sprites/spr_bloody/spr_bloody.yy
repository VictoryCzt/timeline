{
    "id": "f5117eca-9c3a-47e5-841d-eccb1a7ee14a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bloody",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4284ede6-a0df-420d-be35-d919580d0ca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5117eca-9c3a-47e5-841d-eccb1a7ee14a",
            "compositeImage": {
                "id": "b939850a-7348-49da-8e07-d67e6da6e2d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4284ede6-a0df-420d-be35-d919580d0ca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "533464b5-b873-4163-a993-256a79b2c194",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4284ede6-a0df-420d-be35-d919580d0ca8",
                    "LayerId": "de64796b-cfe3-4b33-996f-f259b4c7ae5d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "de64796b-cfe3-4b33-996f-f259b4c7ae5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5117eca-9c3a-47e5-841d-eccb1a7ee14a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}