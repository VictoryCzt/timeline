{
    "id": "b7fe8db4-7991-4bef-ba8d-65fe69edb272",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ammo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 5,
    "bbox_right": 18,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db793637-95be-4459-98a8-adff723a2478",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b7fe8db4-7991-4bef-ba8d-65fe69edb272",
            "compositeImage": {
                "id": "5625add8-6058-4deb-bd43-47db6e8bd945",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db793637-95be-4459-98a8-adff723a2478",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae75dbb7-e699-4286-b4d8-3c38f40b7475",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db793637-95be-4459-98a8-adff723a2478",
                    "LayerId": "eafbaa13-996d-4e10-8d76-2aae7297895d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "eafbaa13-996d-4e10-8d76-2aae7297895d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b7fe8db4-7991-4bef-ba8d-65fe69edb272",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}