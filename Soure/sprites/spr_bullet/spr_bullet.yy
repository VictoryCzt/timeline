{
    "id": "11af9f37-dd12-42df-9580-4b9ab6b3c427",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e4994c4-e3f1-46af-986a-b6aa7779d52a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11af9f37-dd12-42df-9580-4b9ab6b3c427",
            "compositeImage": {
                "id": "2a743b3a-a350-48b0-84f2-17bb60c00199",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e4994c4-e3f1-46af-986a-b6aa7779d52a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef9f6c8f-6221-4af0-bfae-18d79292d882",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e4994c4-e3f1-46af-986a-b6aa7779d52a",
                    "LayerId": "7ae4bc68-f782-4621-bf46-c45d44ffe911"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "7ae4bc68-f782-4621-bf46-c45d44ffe911",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11af9f37-dd12-42df-9580-4b9ab6b3c427",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 2,
    "yorig": 2
}