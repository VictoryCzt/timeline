{
    "id": "8344dfe5-316a-4a1e-862a-77b01407e427",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gun_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 6,
    "bbox_right": 16,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54ce1afb-c5fa-4412-919f-8bdf1a3cbdb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8344dfe5-316a-4a1e-862a-77b01407e427",
            "compositeImage": {
                "id": "3a8fb183-2b83-46b2-bf99-a6562a111613",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54ce1afb-c5fa-4412-919f-8bdf1a3cbdb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6552f320-78fa-4376-a57e-e5ae573ff3ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54ce1afb-c5fa-4412-919f-8bdf1a3cbdb0",
                    "LayerId": "1e02d786-2883-4360-abe4-bc631897d680"
                }
            ]
        },
        {
            "id": "e7329718-8b9a-4b52-8296-37da2cc371ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8344dfe5-316a-4a1e-862a-77b01407e427",
            "compositeImage": {
                "id": "a66372f3-5cf1-41e1-bbcb-274113f6ed52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7329718-8b9a-4b52-8296-37da2cc371ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "381dd792-fdeb-4a0a-8bde-1da5daeff98f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7329718-8b9a-4b52-8296-37da2cc371ab",
                    "LayerId": "1e02d786-2883-4360-abe4-bc631897d680"
                }
            ]
        },
        {
            "id": "b8487f89-2bad-4169-a7a7-ba0286072b4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8344dfe5-316a-4a1e-862a-77b01407e427",
            "compositeImage": {
                "id": "d3bdee8b-0caa-46e8-8b6a-4694327b786c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8487f89-2bad-4169-a7a7-ba0286072b4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c216512-6002-4143-8939-5cb63a9e978d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8487f89-2bad-4169-a7a7-ba0286072b4e",
                    "LayerId": "1e02d786-2883-4360-abe4-bc631897d680"
                }
            ]
        },
        {
            "id": "850544c9-9624-4b50-89ef-999e9e386540",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8344dfe5-316a-4a1e-862a-77b01407e427",
            "compositeImage": {
                "id": "e3d9a65f-6e7b-441e-b7b8-7bd3ddef11a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "850544c9-9624-4b50-89ef-999e9e386540",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eea1f1e5-6337-4eae-82d1-acfac5b64a0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "850544c9-9624-4b50-89ef-999e9e386540",
                    "LayerId": "1e02d786-2883-4360-abe4-bc631897d680"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "1e02d786-2883-4360-abe4-bc631897d680",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8344dfe5-316a-4a1e-862a-77b01407e427",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 11,
    "yorig": 44
}