{
    "id": "916f36f4-cef1-4e32-b05f-7529a50c4508",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3115389f-8e3d-4a94-9091-ba1aa61c332d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "916f36f4-cef1-4e32-b05f-7529a50c4508",
            "compositeImage": {
                "id": "ffbd2bf6-cfd5-4f21-903f-0aad3a6a8428",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3115389f-8e3d-4a94-9091-ba1aa61c332d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "078ca275-4842-43d8-8a86-2d1ad5ce4a9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3115389f-8e3d-4a94-9091-ba1aa61c332d",
                    "LayerId": "6d68cc9c-49f1-468a-9422-523c9297c270"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "6d68cc9c-49f1-468a-9422-523c9297c270",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "916f36f4-cef1-4e32-b05f-7529a50c4508",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": -277,
    "yorig": -110
}