{
    "id": "b74e2161-bafe-4028-aebe-8157de0ff54b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_G",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9412ae48-c173-42f0-b9e5-5081d22b4325",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b74e2161-bafe-4028-aebe-8157de0ff54b",
            "compositeImage": {
                "id": "58c5ef58-2e75-4257-92ff-247f21111ffa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9412ae48-c173-42f0-b9e5-5081d22b4325",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f83419d0-348c-4635-8577-db84db73400e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9412ae48-c173-42f0-b9e5-5081d22b4325",
                    "LayerId": "33a17030-1e6a-4bc6-823a-7cfd1c6e59a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "33a17030-1e6a-4bc6-823a-7cfd1c6e59a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b74e2161-bafe-4028-aebe-8157de0ff54b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}