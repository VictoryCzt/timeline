{
    "id": "27e458a1-af7e-42d4-a6fb-1ba47f17f820",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 47,
    "bbox_right": 219,
    "bbox_top": 47,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "934fce46-011b-43bb-ac06-87d790402b48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27e458a1-af7e-42d4-a6fb-1ba47f17f820",
            "compositeImage": {
                "id": "25bea8a2-531c-4e60-adcc-fa6fa9b34af9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "934fce46-011b-43bb-ac06-87d790402b48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10fda8de-5413-48d9-867d-470ae409bea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "934fce46-011b-43bb-ac06-87d790402b48",
                    "LayerId": "ff7edbe7-02c4-4927-92bc-ba0b8e7d2849"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "ff7edbe7-02c4-4927-92bc-ba0b8e7d2849",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27e458a1-af7e-42d4-a6fb-1ba47f17f820",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}