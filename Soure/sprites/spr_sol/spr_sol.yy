{
    "id": "f25253a9-5e71-4663-b0d3-f83adf4f829e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sol",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 14,
    "bbox_right": 46,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f65fd205-1a5e-4adb-89e1-4b676966c51e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f25253a9-5e71-4663-b0d3-f83adf4f829e",
            "compositeImage": {
                "id": "70f933b2-f9c8-4afb-aa81-59fb58b7e9fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f65fd205-1a5e-4adb-89e1-4b676966c51e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2bfd754-1a18-4fc9-8903-551e2eae1457",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f65fd205-1a5e-4adb-89e1-4b676966c51e",
                    "LayerId": "593ff64b-5675-4239-862c-ca15fd1bd540"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "593ff64b-5675-4239-862c-ca15fd1bd540",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f25253a9-5e71-4663-b0d3-f83adf4f829e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 30,
    "yorig": 33
}