{
    "id": "6c2234d2-b109-432c-bb18-b801f589d337",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_zombie",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 16,
    "bbox_right": 46,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2516ccef-bd9f-497c-8ec7-642cba6dc39a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c2234d2-b109-432c-bb18-b801f589d337",
            "compositeImage": {
                "id": "bdf90379-bf5b-4dbf-aa88-9d0b241c2600",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2516ccef-bd9f-497c-8ec7-642cba6dc39a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56284403-8712-4847-829d-d6cbffba9fb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2516ccef-bd9f-497c-8ec7-642cba6dc39a",
                    "LayerId": "a60ee49f-95c6-43d1-8779-03ea270ce382"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a60ee49f-95c6-43d1-8779-03ea270ce382",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c2234d2-b109-432c-bb18-b801f589d337",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}