{
    "id": "1f4512fa-f770-4c6e-8c7d-42964d2099fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_boom_bag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 2,
    "bbox_right": 22,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f63c1db-26b9-4f32-afc7-1fb7f75091bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f4512fa-f770-4c6e-8c7d-42964d2099fa",
            "compositeImage": {
                "id": "b39df47c-17a6-4089-a753-bd45aa058ade",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f63c1db-26b9-4f32-afc7-1fb7f75091bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b091c01-8fd1-4276-9fb3-eede4dd77708",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f63c1db-26b9-4f32-afc7-1fb7f75091bc",
                    "LayerId": "d1d42406-e463-4b00-a77d-9a41b792758c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "d1d42406-e463-4b00-a77d-9a41b792758c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f4512fa-f770-4c6e-8c7d-42964d2099fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 12
}