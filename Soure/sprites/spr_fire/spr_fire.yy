{
    "id": "78716295-11c8-4e3f-8ba6-32e1ae050e9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4eb9a81a-3fdc-4ca7-b81a-9a016d5e4337",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78716295-11c8-4e3f-8ba6-32e1ae050e9e",
            "compositeImage": {
                "id": "b3af5069-1ae3-4baa-9c01-df844b99e4f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eb9a81a-3fdc-4ca7-b81a-9a016d5e4337",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5ca6c5c-e78a-43d4-a31a-523f9b5a1e74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eb9a81a-3fdc-4ca7-b81a-9a016d5e4337",
                    "LayerId": "65d57c35-d965-4904-9b85-c69126d0db30"
                }
            ]
        },
        {
            "id": "2c459eb6-ee30-4dfb-9c2c-a6bc73057d44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78716295-11c8-4e3f-8ba6-32e1ae050e9e",
            "compositeImage": {
                "id": "2ef45da4-d327-4a1c-9c53-eb03f7fb011f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c459eb6-ee30-4dfb-9c2c-a6bc73057d44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a28eba2-4329-490a-abff-bf2523184ddb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c459eb6-ee30-4dfb-9c2c-a6bc73057d44",
                    "LayerId": "65d57c35-d965-4904-9b85-c69126d0db30"
                }
            ]
        },
        {
            "id": "5c1cb871-debf-4e5d-b52d-8dcfc03ab156",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78716295-11c8-4e3f-8ba6-32e1ae050e9e",
            "compositeImage": {
                "id": "930e2742-383e-4556-b188-e18c6f9c7dd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c1cb871-debf-4e5d-b52d-8dcfc03ab156",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed170d4c-fa93-4b7a-85f9-d18b1039acce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c1cb871-debf-4e5d-b52d-8dcfc03ab156",
                    "LayerId": "65d57c35-d965-4904-9b85-c69126d0db30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "65d57c35-d965-4904-9b85-c69126d0db30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78716295-11c8-4e3f-8ba6-32e1ae050e9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 2,
    "yorig": 7
}