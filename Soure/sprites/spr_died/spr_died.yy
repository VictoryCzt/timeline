{
    "id": "217b9db2-e2b4-4ae5-805c-217e7eb59026",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_died",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 8,
    "bbox_right": 63,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07999db5-5683-4cb7-ab37-7bda8a26a530",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "217b9db2-e2b4-4ae5-805c-217e7eb59026",
            "compositeImage": {
                "id": "6b97e6a8-8bdc-48e0-9e1f-9c6382aaf801",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07999db5-5683-4cb7-ab37-7bda8a26a530",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53d2f067-4028-4f90-99f7-684ae6045a71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07999db5-5683-4cb7-ab37-7bda8a26a530",
                    "LayerId": "3e091460-ccf7-4c15-b70c-cc120717625c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3e091460-ccf7-4c15-b70c-cc120717625c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "217b9db2-e2b4-4ae5-805c-217e7eb59026",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}