{
    "id": "29dedfdf-06a3-4539-811d-b6ad87526ffb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_machine_gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 6,
    "bbox_right": 16,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de3c5e6d-e18f-4648-8879-f42fc985b666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29dedfdf-06a3-4539-811d-b6ad87526ffb",
            "compositeImage": {
                "id": "64c1a21b-51b6-4c82-9692-658919c08d02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de3c5e6d-e18f-4648-8879-f42fc985b666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91486d88-d0e6-4334-924f-a2a8dc686107",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de3c5e6d-e18f-4648-8879-f42fc985b666",
                    "LayerId": "b9a3444b-b4d3-4630-a88a-c5a64450d164"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b9a3444b-b4d3-4630-a88a-c5a64450d164",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29dedfdf-06a3-4539-811d-b6ad87526ffb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 11,
    "yorig": 44
}