{
    "id": "7e3e3018-8828-47fa-b5d0-7259473f2752",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ammo",
    "eventList": [
        {
            "id": "58bfa408-d688-4a8d-b514-3fb2a400e6bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7e3e3018-8828-47fa-b5d0-7259473f2752"
        },
        {
            "id": "0a57f69c-0ac9-427e-a6dc-0dd20a761a5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "755622d7-4200-4c1e-b556-189e52f0ff7d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7e3e3018-8828-47fa-b5d0-7259473f2752"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b7fe8db4-7991-4bef-ba8d-65fe69edb272",
    "visible": true
}