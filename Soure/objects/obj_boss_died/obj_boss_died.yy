{
    "id": "1935d887-f92f-4240-9821-29ec6205d2a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss_died",
    "eventList": [
        {
            "id": "577a97ed-3fcc-42c8-bd88-124e8cfafce6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1935d887-f92f-4240-9821-29ec6205d2a1"
        },
        {
            "id": "259304f0-7e74-4e22-a1d7-8feb1b7285f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1935d887-f92f-4240-9821-29ec6205d2a1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "597ccab1-99bd-47ff-9cc7-fff0a545a223",
    "visible": true
}