{
    "id": "09d6f508-500f-4288-bb75-36e1b2d8c4d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boom_bag",
    "eventList": [
        {
            "id": "af5db2bc-56d4-4623-baa7-59ba94679d0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "09d6f508-500f-4288-bb75-36e1b2d8c4d3"
        },
        {
            "id": "5902433b-61a8-411d-b099-f65fd41819e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "755622d7-4200-4c1e-b556-189e52f0ff7d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "09d6f508-500f-4288-bb75-36e1b2d8c4d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1f4512fa-f770-4c6e-8c7d-42964d2099fa",
    "visible": true
}