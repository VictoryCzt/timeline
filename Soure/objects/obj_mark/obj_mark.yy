{
    "id": "0aed8ee7-564f-4b14-ab3e-e7cfdf14796e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mark",
    "eventList": [
        {
            "id": "f027c565-9e8d-497e-9f89-30e4c82d3dc5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0aed8ee7-564f-4b14-ab3e-e7cfdf14796e"
        },
        {
            "id": "62663793-fd17-402d-960e-b559a12e6aed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0aed8ee7-564f-4b14-ab3e-e7cfdf14796e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "916f36f4-cef1-4e32-b05f-7529a50c4508",
    "visible": true
}