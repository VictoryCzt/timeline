{
    "id": "3a9fe678-b36a-403d-82fd-dc56761a59ab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "07ce1b1e-a3a3-45ef-8b10-bfb13511b70f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3a9fe678-b36a-403d-82fd-dc56761a59ab"
        },
        {
            "id": "d5c27d4b-175a-4ef4-929d-59d07a6e149b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3a9fe678-b36a-403d-82fd-dc56761a59ab"
        },
        {
            "id": "cb8bd648-e36a-4694-abaa-7bc495a3df7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "3a9fe678-b36a-403d-82fd-dc56761a59ab"
        },
        {
            "id": "be647de1-60c8-4d79-9934-cc04413c0beb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "3a9fe678-b36a-403d-82fd-dc56761a59ab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "11af9f37-dd12-42df-9580-4b9ab6b3c427",
    "visible": true
}