{
    "id": "27db3fc9-2058-4eaa-93b2-ba4b98e92c9c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blood",
    "eventList": [
        {
            "id": "488b4a3f-6aa9-4a7a-bfd5-1cdac550e8ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "27db3fc9-2058-4eaa-93b2-ba4b98e92c9c"
        },
        {
            "id": "3c152a15-8444-488a-93e1-c53f37f8a2a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "27db3fc9-2058-4eaa-93b2-ba4b98e92c9c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0fce8646-843d-4830-b489-b560d126ac4e",
    "visible": true
}