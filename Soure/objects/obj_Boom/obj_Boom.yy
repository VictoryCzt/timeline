{
    "id": "fd9bf7f0-ad98-418b-84d5-dd6b95c3279c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Boom",
    "eventList": [
        {
            "id": "b382cd05-6274-442b-ac46-ddf6bfff3d9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fd9bf7f0-ad98-418b-84d5-dd6b95c3279c"
        },
        {
            "id": "fae060db-a27d-4d78-8b44-e1549b844253",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fd9bf7f0-ad98-418b-84d5-dd6b95c3279c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "323db807-4590-45c2-92bc-e2a016c91a0e",
    "visible": true
}