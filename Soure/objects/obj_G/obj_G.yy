{
    "id": "90d034cd-5c4f-4954-994d-c952923df872",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_G",
    "eventList": [
        {
            "id": "04dbd2fe-e128-46a2-a4fa-3d565ed4e360",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "90d034cd-5c4f-4954-994d-c952923df872"
        },
        {
            "id": "7dc22e51-681f-4f93-8bac-db13569171ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "90d034cd-5c4f-4954-994d-c952923df872"
        },
        {
            "id": "2f0ce742-3995-4f2b-a136-5a727ac72678",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "90d034cd-5c4f-4954-994d-c952923df872"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b74e2161-bafe-4028-aebe-8157de0ff54b",
    "visible": true
}