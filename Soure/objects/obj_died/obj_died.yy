{
    "id": "f29bedba-8f8b-4251-a914-8f9f35992400",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_died",
    "eventList": [
        {
            "id": "322eb441-e4cc-48db-8a6f-16351ee5150e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f29bedba-8f8b-4251-a914-8f9f35992400"
        },
        {
            "id": "51866fc4-e256-4e8b-a4ae-31fcf0e25cba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f29bedba-8f8b-4251-a914-8f9f35992400"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "217b9db2-e2b4-4ae5-805c-217e7eb59026",
    "visible": true
}