{
    "id": "5754f9d1-4f79-434f-a537-69bebdfde25c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_zombie",
    "eventList": [
        {
            "id": "552bcd27-2a5f-416d-82f6-15ae89ad487a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5754f9d1-4f79-434f-a537-69bebdfde25c"
        },
        {
            "id": "e1c2f618-102f-43ce-a2aa-783a008fdc73",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3a9fe678-b36a-403d-82fd-dc56761a59ab",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5754f9d1-4f79-434f-a537-69bebdfde25c"
        },
        {
            "id": "42ab6c57-b9ea-4fc0-8f6a-f8411d17f830",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "5754f9d1-4f79-434f-a537-69bebdfde25c"
        },
        {
            "id": "d8ffa675-bcbe-4f23-bf8b-eba6ecb5297b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5754f9d1-4f79-434f-a537-69bebdfde25c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6c2234d2-b109-432c-bb18-b801f589d337",
    "visible": true
}