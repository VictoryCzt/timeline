{
    "id": "755622d7-4200-4c1e-b556-189e52f0ff7d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sol",
    "eventList": [
        {
            "id": "978fe329-30d6-49dc-aad1-9a9d91312b5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "755622d7-4200-4c1e-b556-189e52f0ff7d"
        },
        {
            "id": "412e20e9-6434-4357-b0b7-640100a64a55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "755622d7-4200-4c1e-b556-189e52f0ff7d"
        },
        {
            "id": "1f7ddc6b-7bda-4296-951d-d265d4911fcc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5754f9d1-4f79-434f-a537-69bebdfde25c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "755622d7-4200-4c1e-b556-189e52f0ff7d"
        },
        {
            "id": "ca252207-f957-4655-a781-2ffb7a193331",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9305ac10-bf0f-4710-a37d-5f5eee3de1a1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "755622d7-4200-4c1e-b556-189e52f0ff7d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f25253a9-5e71-4663-b0d3-f83adf4f829e",
    "visible": true
}