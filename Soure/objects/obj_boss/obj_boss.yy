{
    "id": "9305ac10-bf0f-4710-a37d-5f5eee3de1a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss",
    "eventList": [
        {
            "id": "dd8887ce-02cb-4039-b9c2-0cd882431b8b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9305ac10-bf0f-4710-a37d-5f5eee3de1a1"
        },
        {
            "id": "89ebd70c-ded2-4fe1-af83-7ba42d822c38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3a9fe678-b36a-403d-82fd-dc56761a59ab",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9305ac10-bf0f-4710-a37d-5f5eee3de1a1"
        },
        {
            "id": "d3ebd6e7-b6f9-4e08-89d4-2754dc13403c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9305ac10-bf0f-4710-a37d-5f5eee3de1a1"
        },
        {
            "id": "3e22fe61-e3ab-4b14-8502-4ba2eb623754",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "9305ac10-bf0f-4710-a37d-5f5eee3de1a1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "27e458a1-af7e-42d4-a6fb-1ba47f17f820",
    "visible": true
}