{
    "id": "8e0d34a6-0aa9-4833-8854-3a54d3229b19",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bloody",
    "eventList": [
        {
            "id": "6e324b5d-0ba4-4961-a0e8-acfa62fc05b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8e0d34a6-0aa9-4833-8854-3a54d3229b19"
        },
        {
            "id": "0e480d35-50c8-4979-be66-298090eb700d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8e0d34a6-0aa9-4833-8854-3a54d3229b19"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f5117eca-9c3a-47e5-841d-eccb1a7ee14a",
    "visible": true
}